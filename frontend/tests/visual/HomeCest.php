<?php
namespace frontend\tests\visual;

use frontend\tests\VisualTester;
use yii\helpers\Url;

class HomeCest
{
    public function checkHome(VisualTester $I)
    {
        $I->amOnPage(Url::toRoute('/site/index'));
        $I->dontseeVisualChanges('navbar', '#navbar');
    }
}
